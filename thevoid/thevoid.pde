// video export lib and init
import com.hamoid.*;
VideoExport videoExport;

// init an OpenSImplexNoise field
// using "OpenSimplex in Java" by Kurt Spencer
// https://gist.github.com/KdotJPG/b1270127455a94ac5d19
OpenSimplexNoise noise;

// INPUTS
// variables
float startNoiseAmt = 3; // the starting position within the noise field
int blobRadius = 150; // the minimum radius of any blob/circle</code>
// constants
final float rotateSpeed = 0.75; // the speed of the canvas rotation;
final float noiseSpeed = .005; // the speed through the 3d noise field;
final float blobRes = 0.005; // resolution of each blob, lower = more points/detail;
final int blobGap = 7; // the gap between each blob;
final int useSimplexNoise = 1; // SimplexNoise = 1; Perlin Noise = 0;
final int blobToCircle = 1; // blob to circle = 1; circle to blob = 0;
final int useRGB = 1; // RGB color = 1; HSB color = 0;
final int w = 2000; // the width of the image;
final int h = 2000; // the height of the image;

// GLOBALS
color startColor, endColor;
int blobAmt, count;
float startNoisePos, percent, noiseLevel, radius, zoff, totalFrames, n, r, x, y;

// if you want to use variables for width and height, you must assign them in settings(), not setup()
void settings() {
  // smooth is a function that increases detail with anti-aliasing
  smooth(8);
  size(w, h, P2D);
}

void setup() {
  // random number used for starting point in noise field
  //  startNoisePos = floor(random(1000));
  startNoisePos = 160;

  // create a noise object
  noise = new OpenSimplexNoise();

  // set the zoffset, or speed through the noise, == a more readable variable name used as INPUT
  zoff = startNoisePos;

  // set the amount of blobs to draw based on width of frame
  blobAmt = width/4;

  // determine the total amount of frames the animation will take so we can reset and draw again
  // and also so we can map colouring accuratly to the first and end blobs
  totalFrames = blobAmt/blobGap;

  // a counter
  count = 0;

  // a function that will reset the colours to new random values
  resetColors();

  // video export setup
  videoExport = new VideoExport(this);
  videoExport.startMovie();
}

void draw() {

  // move to the center so we are drawing around the frame centerpoint
  translate(width/2, height/2);

  // roatate the frame to add complexity and layering/moire effects to the generated blobs
  rotate(radians(frameCount*rotateSpeed));

  // reset the radius on each main loop
  radius = blobRadius;

  if (blobAmt>0) {

    // determines the direction, blob to circ / circ to blob
    if (blobToCircle==1) {
      noiseLevel = map(count, 0, totalFrames, startNoiseAmt, 0);
    } else {
      noiseLevel = map(count, 0, totalFrames, 0, startNoiseAmt);
    }

    // Draw a grouping of concentric blobs
    for (int i=blobAmt; i>0; i--) {

      // use the totalFrame value to map a gradual fade from black
      percent = map(count, 0, totalFrames, 0, 100);

      // lerpColor determines the colour values to make a gradient
      // it takes N input colours and will output some value between them
      // based on a percentage
      color c = lerpColor(startColor, endColor, percent/100);
      //color c = lerpColor(#1A6FC3, #BA2F06, percent/100);
      stroke(c, percent);

      // push matrix so we can pop back to original draw settings
      pushMatrix();

      // a better way of drawing a circle is to use several points and connect them
      // this allows you to shift those points over time and animate the circle
      // as a blob shape
      beginShape();

      // blobRes will determine the number of points/resolution
      for (float a=0; a < TWO_PI; a+= blobRes) {

        // set the xoffset and yoffset based on the angle of a
        // mapp it to some value between 0 and the NoiseLevel
        // noise level will either increase or decrease based on an INPUT
        // result is either a circle to a blob or a blob to a circle
        float xoff = map(cos(a), -1, 1, 0, noiseLevel);
        float yoff = map(sin(a), -1, 1, 0, noiseLevel);

        // what noise to use, Simplex (smooth) or Perlin (kinda less smooth?)
        if (useSimplexNoise==1) {
          n = (float)(noise.eval(xoff, yoff, zoff));
          r = map(n, -1, 1, 0, radius);
        } else {
          n = noise(xoff, yoff, zoff);
          r = map(n, 0, 1, 0, radius);
        }

        // set the x,y of the vertex point which has been affected by the noise
        // value from above. this offsets the point +/- from the circumfrence of the 
        // original circle. morph each point and you get a blob
        x = r*cos(a);
        y = r*sin(a);

        // draw the vertex
        vertex(x, y);
      }
      // close the shape to ensure a proper closure
      // the start meets the end at the same vertex
      endShape(CLOSE);
      popMatrix();

      // we add to the radius, technically the circles are being drawn out
      // from the center and we only see the last few due to a gradually
      // increasing alpha effact by using a semi-transparent fill
      radius+=blobGap;
    }

    // now we count down from the original blob amount
    // so the following blob grouping will be a bit smaller due to there being less blobs
    // this gives a layer and depth effect
    blobAmt-=blobGap;

    // move though the noise field
    zoff += noiseSpeed;
  }

  // save a frame to the video
  videoExport.saveFrame();

  // if we have reach the end of a full set of blob draws
  // we save an image and reset everything to begin again
  // this will continously run and generate images until you stop
  if (count > totalFrames) {
    saveFrameAndReset();
    //    noLoop();
  } else {
    // if its not at the end, add one to the counter and loop
    count++;
  }
}

// a function that resets all the INPUTS
// and saves the frame to an image
// all INPUT values are saved to the filename for recreation purposes
void saveFrameAndReset() {
  saveFrame("rgb"+startNoiseAmt+
    "_namt"+startNoiseAmt+
    "_npos"+startNoisePos+
    "_nspd"+noiseSpeed+
    "_rspd"+rotateSpeed+
    "_bgap"+blobGap+
    "_brad"+blobRadius+
    "_smpn"+useSimplexNoise+
    "_btoc"+blobToCircle+
    "_"+hex(startColor, 6)+
    "-"+hex(endColor, 6)+
    ".jpg");
  count=0;
  startNoisePos += 1;
  zoff = startNoisePos;
  blobAmt = width/4;
  radius = 1;
  resetColors();
}

// a function that resets the colours based on which
// colour space is used as INPUT
void resetColors() {
  if (useRGB==1) {
    makeRGBColors();
    background(7);
    fill(0, 7);
  } else {
    makeHSBColors();
    background(7);
    fill(0, 7);
  }
}

// generate random RGB values
void makeRGBColors() {
  colorMode(RGB, 255, 255, 255, 100);
  startColor = color(random(255), random(255), random(255));
  endColor = color(random(255), random(255), random(255));
}

// generate random HSB values
void makeHSBColors() {
  colorMode(HSB, 360, 100, 100, 100);
  float c = random(360);
  startColor = color(random(-20, 20) + c % 360, random(80, 100), random(90, 100));
  endColor =   color(random(-20, 20) + c % 180, random(70, 90), random(90, 100));
}

// stop
void keyPressed() {
  if (key == 'q') {
    videoExport.endMovie();
    exit();
    noLoop();
  }
}
